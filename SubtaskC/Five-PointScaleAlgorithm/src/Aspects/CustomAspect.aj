package Aspects;

/**
 * Created by Cosmin on 04.01.2016.
 */
public aspect CustomAspect {
    pointcut print() : execution (* EmoticonClassification.EmoticonClassifier.printEmoticonList());

    before(): print() {
        System.out.println("\nStart of emoticon list");
    }

    after() returning() : print() {
        System.out.println("End of emoticon list");
    }
}
