package Final;

/**
 * Created by Cosmin on 25.11.2015.
 */
import EmoticonClassification.EmoticonClassifier;
import NaiveBayes.NaiveBayes;

import java.io.FileNotFoundException;

public class AlgChooser {
    NaiveBayes nb;
    EmoticonClassifier em;

    /**
     * This class was ment to work as an Adapeter
     * Add other alg here as objects
     */



    public AlgChooser() throws FileNotFoundException {
        nb = nb.getInstance();
        em = em.getInstance();

    }


    public String predict(String tweet){

        switch(bestAlg(tweet)){
            case 1:return nb.predict(tweet);
            case 2:return em.predict(tweet);
            default:break;
        }

        return new String("result");
    }

    /**
     * decide which classifier works best
     */
    private int bestAlg(String tweet){
        int bestAlg=0;

        return bestAlg;
    }

}

