package EmoticonClassification;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by Cosmin on 25.11.2015.
 */
public class EmoticonClassifier {


    static EmoticonClassifier instanceOf=null;
    public static HashMap<String,String> emoticonList = new HashMap<String,String>();

    public EmoticonClassifier() throws FileNotFoundException {

       // emoticonList.put();


        try (Scanner scanner = new Scanner(new File("emoticons.txt"))) {

            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                String[] split = line.split(" ");
                String emoticon = split[0];
                String classification=split[1];
                emoticonList.put(emoticon, classification);
            }

            scanner.close();

        }
    }


    public EmoticonClassifier getInstance() throws FileNotFoundException {
        if(instanceOf == null) {
            instanceOf = new EmoticonClassifier();
        }

        return instanceOf;
    }


    public String predict(String tweet) {
        String result = "-4";
        Iterator it = emoticonList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            if(tweet.contains(pair.getKey().toString())){
                return pair.getValue().toString();
            }
        }
        return result;
    }


    public void printEmoticonList(){
        Iterator it = emoticonList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
        }
    }
    public boolean containsEmoticon(String text){
        Iterator it = emoticonList.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry pair = (Map.Entry)it.next();
            if(text.contains(pair.getKey().toString())){
                return true;
            }
        }
        return false;
    }
}
