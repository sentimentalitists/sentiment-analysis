package NaiveBayes;

import EmoticonClassification.EmoticonClassifier;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.*;

public class MainClass {

    public static void main(String[] args) {
        String[] dataSetA = {"/datasets/ATrainGold.tsv", "/datasets/ADevGold.tsv"};
        String dataTestA = "/datasets/ADevTestGold.tsv";
        AlgorithmExecution("subtaskA", dataSetA, dataTestA);

        String[] dataSetBD = {"/datasets/BDTrainGold.tsv", "/datasets/BDDevGold.tsv"};
        String dataTestBD = "/datasets/BDDevTestGold.tsv";
        AlgorithmExecution("subtaskB", dataSetBD, dataTestBD);

        String[] dataSetCE = {"/datasets/CETrainGold.tsv", "/datasets/CEDevGold.tsv", "/datasets/devCE.tsv"};
        String dataTestCE = "/datasets/CEDevTestGold.tsv";
        AlgorithmExecution("subtaskC", dataSetCE, dataTestCE);

    }

    public static String[] readLines(URL url) throws IOException {

        Reader fileReader = new InputStreamReader(url.openStream(), Charset.forName("UTF-8"));
        List<String> lines;
        try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            lines = new ArrayList<>();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                lines.add(line);
            }
        }
        return lines.toArray(new String[lines.size()]);
    }

    public static void AlgorithmExecution(String subtask, String[] dataSet, String dataTest) {
        //map of dataset files
        Map<String, URL> trainingFiles = new HashMap<>();
        for (int i = 0; i < dataSet.length; ++i) {
            trainingFiles.put(subtask + i, MainClass.class.getResource(dataSet[i]));
        }

        //loading examples in memory
        Map<String, String[]> trainingExamples = new HashMap<>();
        for (Map.Entry<String, URL> entry : trainingFiles.entrySet()) {
            try {
                trainingExamples.put(entry.getKey(), readLines(entry.getValue()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //train classifier
        NaiveBayes nb = new NaiveBayes();
        nb.setChisquareCriticalValue(6.63);
        nb.train(trainingExamples);

        //get trained classifier knowledgeBase
        NaiveBayesKnowledgeBase knowledgeBase = nb.getKnowledgeBase();

        nb = null;
        trainingExamples = null;

        //Use classifier
        nb = new NaiveBayes(knowledgeBase);

        Map<String, URL> testingFile = new HashMap<>();
        testingFile.put("TweetsTesting", MainClass.class.getResource(dataTest));

        Map<String, String[]> test = new HashMap<>();
        for (Map.Entry<String, URL> entry : testingFile.entrySet()) {
            try {
                test.put(entry.getKey(), readLines(entry.getValue()));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String[] sentences;
        int nr = 0;
//        int number_notAvailable = 0;
        String filename = "Output " + subtask + ".tsv";
        FileWriter fw = null;
        try {
            fw = new FileWriter(filename, false);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Iterator<Map.Entry<String, String[]>> it = test.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String[]> entry = it.next();
            sentences = entry.getValue();
            for (int i = 0; i < sentences.length; ++i) {

                String text = null;
                String[] fields = sentences[i].split("\t", -1);

                if(subtask.contains("subtaskA")){
                    text = fields[2];
                    String output = nb.predict(text);
                    String expectedResult = fields[1];
                    if (!output.equals(expectedResult) && !(text.equals("Not Available"))) {
                        nr++;
                    }
                    try{
                        fw.write(fields[0]);
                        fw.write("\t");
                        fw.write(output);
                        fw.write("\t");
                        fw.write(fields[2]);
                        fw.write("\n");
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
                else if(subtask.contains("subtaskB") || subtask.contains("subtaskC")){
                    text = fields[3];
                    String output = nb.predict(text);
                    String expectedResult = fields[2];
                    if (!output.equals(expectedResult) && !(text.equals("Not Available"))) {
                        nr++;
                    }
                    try{
                        fw.write(fields[0]);
                        fw.write("\t");
                        fw.write(fields[1]);
                        fw.write("\t");
                        fw.write(output);
                        fw.write("\t");
                        fw.write(fields[3]);
                        fw.write("\n");
                    }catch (IOException e)
                    {
                        e.printStackTrace();
                    }
                }
            }
            try {
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            int x = nr * 100 / (sentences.length);
            System.out.println("\n"+subtask+" : Probability to fail: "+ x +'%');

        }
        getStatisticOfAlgorithm(subtask, test, nb);
    }

    public static void getStatisticOfAlgorithm(String subtask, Map<String, String[]> test, NaiveBayes nb){

        String[] sentences;
        int nr = 0;
        int total = 0;
        int total_emmoticons = 0;
        int failed_emmoticons_nb = 0;

        int[] extremly_negative_fail = new int[5];
        int extremly_negative_classification_fail = 0;

        int[] negative_fail = new int[5];
        int negative_classification_fail = 0;

        int[] neutral_fail = new int[5];
        int neutral_classification_fail = 0;

        int positive_classification_fail = 0;
        int[] positive_fail = new int[5];

        int[] extremely_positive_fail = new int[5];
        int extremely_positive_classification_fail = 0;

        // end of statistici

        // statistici
        try {
            EmoticonClassifier em = new EmoticonClassifier();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int nr_emoticon = EmoticonClassifier.emoticonList.size();
        String[][] emoticon_statistics = new String[nr_emoticon][6];

        Iterator it_map = EmoticonClassifier.emoticonList.entrySet().iterator();
        int k = 0;
        while (it_map.hasNext()) {
            Map.Entry pair = (Map.Entry) it_map.next();
            emoticon_statistics[k][0] = pair.getKey().toString();
            emoticon_statistics[k][1] = pair.getValue().toString();
            k++;
        }

        Iterator<Map.Entry<String, String[]>> it = test.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String[]> entry = it.next();
            sentences = entry.getValue();
            //errors for subtaskA
            if(subtask.contains("subtaskA")){
                    for (int i = 0; i < sentences.length; ++i) {
                        String text = null;
                        String[] fields = sentences[i].split("\t", -1);
                        text = fields[2];
                        String output = nb.predict(text);
                        String expectedResult = fields[1];
                        if (!output.equals(expectedResult) && !(text.equals("Not Available"))) {
                            nr++;
                            switch (expectedResult) {
                                case "negative": {
                                    negative_classification_fail++;
                                    if (output.equals("negative")) negative_fail[0]++;
                                    else if (output.equals("neutral")) negative_fail[1]++;
                                    else negative_fail[2]++;
                                    break;
                                }
                                case "positive": {
                                    positive_classification_fail++;
                                    if (output.equals("negative")) positive_fail[0]++;
                                    else if (output.equals("neutral")) positive_fail[1]++;
                                    else positive_fail[2]++;
                                    break;
                                }
                                case "neutral": {
                                    neutral_classification_fail++;
                                    if (output.equals("negative")) neutral_fail[0]++;
                                    else if (output.equals("neutral")) neutral_fail[1]++;
                                    else neutral_fail[2]++;
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                    }
                    System.out.println("\nSubtask A statistics:");
                    System.out.println("\n\tThe total number of failed classifications is:  " + nr +".");

                    System.out.println("\n\tNEGATIVE(-1) classification fails: " + negative_classification_fail+ ".");
                    System.out.println("\tWhere: ");
                    for(int j=0;j<3;j++){
                        System.out.println("\t"+ negative_fail[j] + " were wrongly classified as " + (j-1));
                    }

                    System.out.println("\n\tNEUTRAL(0) classification fails: " + neutral_classification_fail+ ".");
                    System.out.println("\tWhere: ");
                    for(int j=0;j<3;j++){
                        System.out.println("\t"+ neutral_fail[j] + " were wrongly classified as " + (j-1));
                    }

                    System.out.println("\n\tPOSITIVE(1) classification fails: " + positive_classification_fail+ ".");
                    System.out.println("\tWhere: ");
                    for(int j=0;j<3;j++){
                        System.out.println("\t"+ positive_fail[j] + " were wrongly classified as " + (j-1));
                    }
                }
                //errors for subtaskB
                else if (subtask.contains("subtaskB")){
                    for (int i = 0; i < sentences.length; ++i) {

                        String text = null;
                        String[] fields = sentences[i].split("\t", -1);
                        text = fields[3];
                        String output = nb.predict(text);
                        String expectedResult = fields[2];
                        if (!output.equals(expectedResult) && !(text.equals("Not Available"))) {
                            nr++;
                            switch (expectedResult) {
                                case "negative": {
                                    negative_classification_fail++;
                                    if (output.equals("negative")) negative_fail[0]++;
                                    else negative_fail[1]++;
                                    break;
                                }
                                case "positive": {
                                    positive_classification_fail++;
                                    if (output.equals("negative")) positive_fail[0]++;
                                    else positive_fail[1]++;
                                    break;
                                }
                                default:
                                    break;
                            }
                        }
                    }
                    System.out.println("\nSubtask B statistics:");
                    System.out.println("\n\tThe total number of failed classifications is:  " + nr +".");

                    System.out.println("\n\tNEGATIVE(0) classification fails: " + negative_classification_fail+ ".");
                    System.out.println("\tWhere: ");
                    for(int j=0;j<2;j++){
                        System.out.println("\t"+negative_fail[j] + " were wrongly classified as " + (j));
                    }

                    System.out.println("\n\tPOSITIVE(1) classification fails: " + positive_classification_fail+ ".");
                    System.out.println("\tWhere: ");
                    for(int j=0;j<2;j++){
                        System.out.println("\t"+positive_fail[j] + " were wrongly classified as " + (j));
                    }
                }
                //errors for subtaskC
                else if(subtask.contains("subtaskC")){
                for (int i = 0; i < sentences.length; ++i) {

                    String text = null;
                    String[] fields = sentences[i].split("\t", -1);
                    text = fields[3];
                    String output = nb.predict(text);

                    String expectedResult = fields[2];
                    if (!output.equals(expectedResult) && !(text.equals("Not Available"))) {
                        nr++;
                        switch (expectedResult) {
                            case "0": {
                                neutral_classification_fail++;
                                neutral_fail[Integer.parseInt(output) + 2]++;
                                break;
                            }
                            case "1": {
                                positive_classification_fail++;
                                positive_fail[Integer.parseInt(output) + 2]++;
                                break;
                            }
                            case "2": {
                                extremely_positive_classification_fail++;
                                extremely_positive_fail[Integer.parseInt(output) + 2]++;
                                break;
                            }
                            case "-1": {
                                negative_classification_fail++;
                                negative_fail[Integer.parseInt(output) + 2]++;
                                break;
                            }
                            case "-2": {
                                extremly_negative_classification_fail++;
                                extremly_negative_fail[Integer.parseInt(output) + 2]++;
                                break;
                            }
                            default:
                                break;
                        }
                    }
                }
                    try {
                        EmoticonClassifier emc = new EmoticonClassifier();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    System.out.println("\nSubtask C statistics:");
                    System.out.println("\n\tThe total number of failed classifications is:  " + nr + ".");

                    System.out.println("\n\tEXTREMELY NEGATIVE(-2) classification fails: " + extremly_negative_classification_fail + ".");
                    System.out.println("\tWhere: ");
                    for (int j = 0; j < 5; j++) {
                        System.out.println("\t"+extremly_negative_fail[j] + "\t were wrongly classified as " + (j - 2));
                    }

                    System.out.println("\n\tNEGATIVE(1) classification fails: " + negative_classification_fail + ".");
                    System.out.println("\tWhere: ");
                    for (int j = 0; j < 5; j++) {
                        System.out.println("\t"+negative_fail[j] + "\t were wrongly classified as " + (j - 2));
                    }


                    System.out.println("\n\tNEUTRAL(0) classification fails: " + neutral_classification_fail + ".");
                    System.out.println("\tWhere: ");
                    for (int j = 0; j < 5; j++) {
                        System.out.println("\t"+neutral_fail[j] + "\t were wrongly classified as " + (j - 2));
                    }

                    System.out.println("\n\tPOSITIVE(1) classification fails: " + positive_classification_fail + ".");
                    System.out.println("\tWhere: ");
                    for (int j = 0; j < 5; j++) {
                        System.out.println("\t"+positive_fail[j] + "\t were wrongly classified as " + (j - 2));
                    }

                    System.out.println("\n\tEXTREMELY POSITIVE(2) classification fails: " + extremely_positive_classification_fail + ".");
                    System.out.println("\tWhere: ");
                    for (int j = 0; j < 5; j++) {
                        System.out.println("\t"+extremely_positive_fail[j] + "\t were wrongly classified as " + (j - 2));
                    }
                }
            }
        }
}
