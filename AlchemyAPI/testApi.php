<?php
require_once 'alchemyapi.php';
$alchemyapi = new AlchemyAPI();

$myfile = fopen("output.txt", "w");

$myText = "My acting debut in @B2WFilm was released this weekend. You can check out 'Between Two Worlds' on iTunes here:  https://itun.es/gb/HcjG  🎬🎥😘";
$response = $alchemyapi->sentiment("text", $myText, null);
echo "Sentiment1: ", $response["docSentiment"]["type"], PHP_EOL;
$sentiment1 = "Sentiment1: " . $response["docSentiment"]["type"] . "\n";
fwrite($myfile, $sentiment1);
$myText = "Thanks for having me. A real pleasure to come back  ";
$response = $alchemyapi->sentiment("text", $myText, null);
echo "Sentiment2: ", $response["docSentiment"]["type"], PHP_EOL;
$sentiment2 = "Sentiment2: " . $response["docSentiment"]["type"] . "\n";
fwrite($myfile, $sentiment2);
$myText = "When someone you're meant to be doing business with says to you sorry for the delay I can't remember my email password  ";
$response = $alchemyapi->sentiment("text", $myText, null);
echo "Sentiment3: ", $response["docSentiment"]["type"], PHP_EOL;
$sentiment3 = "Sentiment3: " . $response["docSentiment"]["type"] . "\n";
fwrite($myfile, $sentiment3);

fclose($myfile);


?>
