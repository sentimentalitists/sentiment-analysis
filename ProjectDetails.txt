Project name: Sentiment Analysis in Twitter (Semeval 2016 Task 4)
Team name: Sentimental ITists
Coordinator: Diana Trandabat
Members: 
- Eduard Apostol 
- Oana Bejenaru 
- Octavian Ciobanu 
- Cosmin Florean (cosmin.florean@yahoo.com)
- Ionut Galatanu (galatanu.ionut@gmail.com)
- Ionut Goldan (gold.ionut@gmail.com)
