# Project name 
Sentiment Analysis in Twitter (Semeval 2016 Task 4)
[Google Drive Link](https://drive.google.com/open?id=0BxWhla8Rf7XaNlQzdHdzWmQtNjg)
# Team name  
Sentimental ITists
# Coordinator  
- Lector, Dr. Diana Trandabat
# Members  
- Eduard Apostol (eduardpstl@gmail.com)
- Oana Bejenaru (bejenaru.oana.6@gmail.com)
- Octavian Ciobanu (Octaku8@gmail.com)
- Cosmin Florean (kosmyn01@gmail.com)