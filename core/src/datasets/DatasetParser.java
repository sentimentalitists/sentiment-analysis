package datasets;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sentimentweets.Tweet;
import sentimentweets.TweetBuilder;
import sentimentweets.TweetBuilder.TweetInputType;

/**
 * 
 * @author Ionut Galatanu Singleton with lazy eval class for parsing dataset
 */
public class DatasetParser {
	private static DatasetParser instance = null;

	private DatasetParser() {
	}

	/**
	 * @return instance of dataset parser. Implements lazy evaluation
	 */
	public static DatasetParser getInstance() {
		if (instance == null)
			instance = new DatasetParser();
		return instance;
	}

	/**
	 * Reads input file and returns a list of it's lines
	 * 
	 * @param inputFile
	 *            path of input file
	 * @return list of lines from input file
	 */
	private List<String> readTweetsFromFile(String inputFile) {
		BufferedReader br = null;
		List<String> lines = new ArrayList<>();
		try {
			String line;
			br = new BufferedReader(new FileReader(inputFile));
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
		} catch (FileNotFoundException e) {
			System.out.println("Error: Inexistent file");
		} catch (IOException e) {
			System.out.println("Error: Read line failed");
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("Error: close failed");
				}
		}
		removeBadInput(lines);
		return lines;
	}

	/**
	 * Removes those lines which didn't succed in taking tweet text. Those lines
	 * are marked as Not Available
	 * 
	 * @param lines
	 *            list of lines from input file
	 */
	private void removeBadInput(List<String> lines) {
		Iterator<String> i = lines.iterator();
		while (i.hasNext()) {
			if (i.next().endsWith("\\tNot Available"))
				lines.remove(i);
		}
	}

	public List<Tweet> parseDocument(String inputFile, TweetInputType inputType) {
		List<Tweet> tweets = new ArrayList();
		List<String> lines = readTweetsFromFile(inputFile);
		switch (inputType) {
		case TWEET_COMPLETE:
			return parseTweetsComplete(lines);
		case TWEET_W_CLASSIFICATION:
			return parseTweetsWClassif(lines);
		case TWEET_W_TOPIC:
			return parseTweetsWTopic(lines);
		default:
			return parseTweetsBasic(lines);
		}
	}

	public List<Tweet> parseTweetsBasic(List<String> tweetStrings) {
		List<Tweet> tweets = new ArrayList();
		TweetBuilder tBuilder = new TweetBuilder();
		for (String tweetString : tweetStrings)
			tweets.add(tBuilder.buildTweetBasic(tweetString));
		return tweets;
	}

	public List<Tweet> parseTweetsWClassif(List<String> tweetStrings) {
		List<Tweet> tweets = new ArrayList();
		TweetBuilder tBuilder = new TweetBuilder();
		for (String tweetString : tweetStrings)
			tweets.add(tBuilder.buildTweetWClassif(tweetString));
		return tweets;
	}

	public List<Tweet> parseTweetsWTopic(List<String> tweetStrings) {
		List<Tweet> tweets = new ArrayList();
		TweetBuilder tBuilder = new TweetBuilder();
		for (String tweetString : tweetStrings)
			tweets.add(tBuilder.buildTweetWTopic(tweetString));
		return tweets;
	}

	public List<Tweet> parseTweetsComplete(List<String> tweetStrings) {
		List<Tweet> tweets = new ArrayList();
		TweetBuilder tBuilder = new TweetBuilder();
		for (String tweetString : tweetStrings)
			tweets.add(tBuilder.buildTweetComplete(tweetString));
		return tweets;
	}

}
