package tokenizer;

public abstract class Tokenizer implements TokenizerInterface {
	protected String text;
	protected String [] tokens;
	
	protected void doAllSteps() {
		String escapedText = escape(this.text);
		this.tokens = tokenize(escapedText);
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	public String [] getTokens() {
		return this.tokens;
	}
	
	public Tokenizer () {
	}
	
	public Tokenizer(String text) {
		this.text = text;
	}
}
