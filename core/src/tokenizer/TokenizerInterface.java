package tokenizer;

public interface TokenizerInterface {
	 String escape(String text);
	 
	 String [] tokenize(String text);
}
