package tokenizer;

public class TokenizerFactory {
	public static TokenizerInterface getTokenizer(String tokenizer) throws Exception {
		switch(tokenizer) {
			case "Ascii": return getAsciiTokenizer();
			case "Whitespace": return getWhitespaceTokenizer();
			default: throw new Exception("Could not get the tokenizer type.");
		}
	}
	
	public static TokenizerInterface getTokenizer(Tokenizers tokenizer) throws Exception {
		switch(tokenizer) {
			case Ascii: return getAsciiTokenizer();
			case Whitespace: return getWhitespaceTokenizer();
			default: throw new Exception("Could not get the tokenizer type.");
		}
	}
	
	private static AsciiTokenizer getAsciiTokenizer() {
		return AsciiTokenizer.getInstance();
	}
	
	private static WhitespaceTokenizer getWhitespaceTokenizer() {
		return WhitespaceTokenizer.getInstance();
	}
}
