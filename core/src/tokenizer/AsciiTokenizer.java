/**
 * 
 */
package tokenizer;

import java.util.Locale;

/**
 * @author eduard
 *
 */
public class AsciiTokenizer extends Tokenizer implements TokenizerInterface {

	private static AsciiTokenizer instanceOf;
	
	public static AsciiTokenizer getInstance() {
		if(instanceOf == null) instanceOf = new AsciiTokenizer();
		return instanceOf;
	}
	
	@Override
	public String escape(String text) {
		return text.replaceAll("[^\\x00-\\x7F]", " ").replaceAll("\\s+", " ").toLowerCase(Locale.getDefault());
	}
	
	@Override
	public String[] tokenize(String text) {
		return text.split(" ");
	}

}
