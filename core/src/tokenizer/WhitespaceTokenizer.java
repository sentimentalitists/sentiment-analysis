/**
 * 
 */
package tokenizer;

import java.util.Locale;

/**
 * @author eduard
 *
 */
public class WhitespaceTokenizer extends Tokenizer implements TokenizerInterface {

	private static WhitespaceTokenizer instanceOf;
	
	public static WhitespaceTokenizer getInstance() {
		if(instanceOf == null) instanceOf = new WhitespaceTokenizer();
		return instanceOf;
	}
	
	@Override
	public String escape(String text) {
		return text.replaceAll("\\p{P}", " ").replaceAll("\\s+", " ").toLowerCase(Locale.getDefault());
	}

	@Override
	public String[] tokenize(String text) {
		return text.split(" ");
	}
}
