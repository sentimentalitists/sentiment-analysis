package sentimentweets;

public class Tweet {
	protected String id = "";
	protected String body = "";
	protected String classification = "";
	protected String topic = "";
	
	public Tweet(String id, String body) {
		this.id = id;
		this.body = body;
	}
	
	public Tweet(String id, String body, String topic, String classification) {
		this(id,body);
		this.topic = topic;
		this.classification = classification;
	}
	
	public void setClassification(String classification) {
		this.classification = classification;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getId() {
		return id;
	}
	public String getBody() {
		return body;
	}
	public String getClassification() {
		return classification;
	}
	public String getTopic() {
		return topic;
	}
}
