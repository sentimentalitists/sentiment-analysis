package sentimentweets;

/**
 * 
 * @author Ionut Galatanu
 *
 */

public class TweetBuilder {
	public static enum TweetInputType {
		TWEET_BASIC, TWEET_W_CLASSIFICATION, TWEET_W_TOPIC, TWEET_COMPLETE
	}

	/**
	 * Gets a String with id and tweet text and builds a basic tweet.
	 * 
	 * @param input
	 *            id + tweet text sepparated by a tab character
	 * @return Tweet with information
	 */
	public Tweet buildTweetBasic(String input) {
		String[] splitIn = input.split("\t");

		return new Tweet(splitIn[0], splitIn[1]);
	}

	/**
	 * Gets a String with id, classification tweet text and builds a tweet.
	 * 
	 * @param input
	 *            id + tweet text sepparated by a tab character
	 * @return Tweet with information
	 */
	public Tweet buildTweetWClassif(String input) {
		String[] splitIn = input.split("\t");

		return new Tweet(splitIn[0], splitIn[2], "", splitIn[1]);
	}

	/**
	 * Gets a String with id, topic and tweet text and builds a tweet.
	 * 
	 * @param input
	 *            id + tweet text sepparated by a tab character
	 * @return Tweet with information
	 */
	public Tweet buildTweetWTopic(String input) {
		String[] splitIn = input.split("\t");

		return new Tweet(splitIn[0], splitIn[2], splitIn[1], "");
	}

	/**
	 * Gets a String with id, topic, classification and tweet text and builds a
	 * tweet.
	 * 
	 * @param input
	 *            id + tweet text sepparated by a tab character
	 * @return Tweet with information
	 */
	public Tweet buildTweetComplete(String input) {
		String[] splitIn = input.split("\t");

		return new Tweet(splitIn[0], splitIn[3], splitIn[1], splitIn[2]);
	}
}
