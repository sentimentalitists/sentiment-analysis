public interface IClassifier {

  public void classify();

  public void output(Outputer o );

}